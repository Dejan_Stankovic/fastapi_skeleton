DEFAULT_GOAL := help

DB_VOLUME = postgres_fastapi_db_data
DOCKER_COMPOSE_FILE = docker-compose.yml

### Services ###
APP_SERVICE_NAME = fastapi-app
DB_SERVICE_NAME = fastapi-db

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: clean
clean: clean_pyc

.PHONY: clean_pyc
clean_pyc: ## Clean all *.pyc in the system
	find . -type f -name "*.pyc" -delete || true

.PHONY: prepare_local_env
prepare_local_env: ## Install requirements locally
	( \
		python3 -m venv env; \
		source env/bin/activate; \
		pip3 install -r requirements.txt; \
	)


### App ###
start_app:
	@docker-compose -f ${DOCKER_COMPOSE_FILE} up -d ${APP_SERVICE_NAME}

stop_app:
	@docker-compose -f ${DOCKER_COMPOSE_FILE} stop ${APP_SERVICE_NAME}

restart_app:
	@make stop_app
	@make start_app

logs_app:
	@docker logs ${APP_SERVICE_NAME} -f

cli_app:
	@docker exec -it ${APP_SERVICE_NAME} bash

### DB ###
start_db:
	@docker-compose -f ${DOCKER_COMPOSE_FILE} up -d ${DB_SERVICE_NAME}

stop_db:
	@docker-compose -f ${DOCKER_COMPOSE_FILE} stop ${DB_SERVICE_NAME}

restart_db:
	@make stop_db
	@make start_db

logs_db:
	@docker logs ${DB_SERVICE_NAME} -f

cli_db:
	@docker exec -it ${DB_SERVICE_NAME} bash

### Network ###
create_network:
	@docker network create fastapi-network

### General ###
init:
	@docker network create fastapi-network
	@make build

build:
	@docker-compose -f ${DOCKER_COMPOSE_FILE} build

status:
	@docker-compose -f ${DOCKER_COMPOSE_FILE} ps

clean:
	@make stop
	@docker volume rm ${DB_VOLUME}
	@make start

start:
	@docker-compose -f ${DOCKER_COMPOSE_FILE} up  -d

stop:
	@docker-compose -f ${DOCKER_COMPOSE_FILE} down || true

### prestart and tests ###
prestart:
	@sh ./prestart.sh

tests:
	@sh ./scripts/test-cov-html.sh

ifndef VERBOSE
.SILENT:
endif
