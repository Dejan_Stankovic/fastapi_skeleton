"""create user table

Revision ID: 422895f66379
Revises: 
Create Date: 2023-03-21 16:20:00.393914

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "422895f66379"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "user",
        sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
        sa.Column("name", sa.String(50), nullable=True),
        sa.Column("surname", sa.String(50), nullable=True),
        sa.Column("age", sa.Integer, nullable=True),
    )


def downgrade() -> None:
    op.drop_table("user")
