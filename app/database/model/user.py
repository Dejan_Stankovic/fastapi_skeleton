from typing import Any

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base

Base: Any = declarative_base()


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    surname = Column(String)
    age = Column(Integer)

    def __init__(self, **kwargs):
        self.name = kwargs.get("name")
        self.surname = kwargs.get("surname")
        self.age = kwargs.get("age")
