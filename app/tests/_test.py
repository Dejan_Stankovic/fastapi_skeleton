import json
import os

from fastapi.testclient import TestClient

from main import app

client = TestClient(app)

headers = {os.getenv("API_KEY_NAME"): os.getenv("API_KEY_VALUE")}
body = json.load(open("app/tests/body_example.json", "r"))


def test_healty_check():
    response = client.get("/status")
    assert response.status_code == 200
    assert response.json() == {"status": "ok"}


def test_add_user_response():
    response = client.post("/user", headers=headers, json=body)
    assert response.status_code == 200
    content: dict = response.json()
    assert content.get("id")
    assert type(content.get("id")) == int


def test_predictions_response_time():
    response = client.post("/user", headers=headers, json=body)
    assert response.status_code == 200
    assert float(response.headers.get("x-process-time")) < 0.1


def test_predictions_no_headers():
    response = client.post("/user", json=body)
    assert response.status_code == 401
    assert response.json() == {"detail": "Unauthorized"}
