import logging
import time

import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI, Request

# load configuration
load_dotenv(dotenv_path="app/.env.local")

logging.basicConfig(level=logging.DEBUG)

from app.api.endpoints.user_endpoints import router

app = FastAPI()
app.include_router(router)


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time: float = time.time()
    response = await call_next(request)
    process_time: float = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    logging.debug(f"Request took {process_time} seconds to process.")
    return response


if __name__ == "__main__":
    uvicorn.run("main:app", port=8000, reload=True)
