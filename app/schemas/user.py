from pydantic import BaseModel, Field


class User(BaseModel):
    name: str = Field(..., alias="name")
    surname: str = Field(..., alias="surname")
    age: int = Field(..., alias="age")
