import os

from fastapi import APIRouter, Body, Depends, HTTPException, Security
from fastapi.openapi.models import APIKey
from fastapi.security.api_key import APIKeyHeader
from sqlalchemy.orm import Session

from app import schemas
from app.database import model
from app.database.database import get_db

api_key_header = APIKeyHeader(
    name=os.getenv("API_KEY_NAME", "API_KEY_NAME_NOT_SET"), auto_error=False
)

router = APIRouter()


async def get_api_key(
    api_key_header: str = Security(api_key_header),
):
    if api_key_header == os.getenv("API_KEY_VALUE"):
        return api_key_header

    raise HTTPException(status_code=401)


@router.get("/status")
async def status():
    return {"status": "ok"}


@router.post("/user")
async def add_user(
    body: schemas.User = Body(...),
    db: Session = Depends(get_db),
    _: APIKey = Depends(get_api_key),
):
    user = model.User(**body.dict())
    db.add(user)
    db.commit()
    db.refresh(user)

    return {"id": user.id, "status": "ok"}
